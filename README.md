# Pumpkin Catch #

*Pumpkin Catch* is a game developed in Unity for a 48-hour game jam at the University of Michigan. The theme for this jam was "the hunt". Since the game took place around Hallowe'en, we decided to make a Hallowe'en-themed game.

In the game, one person plays as a scarecrow while the other plays as a pumpkin creature.

Our team took fourth place in the competition, and won an award for "Scariest Game".

The game is currently unplayable due to Unity updates, but feel free to investigate the scenes and source code.

## Viewing the Program ##

1. Clone this repository to your local disk
2. Open the "Unity" folder using Unity3D
3. Explore the scenes, assets and scripts involved in the game